package br.com.mjv.employee.controller.request;

import br.com.mjv.employee.model.Employee;
import lombok.*;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmployeeRequest {
    private String name;
    private String departmentName;
    private String projectName;
    private Integer floor;
    private BigDecimal salary;
    public Employee converter(Employee employee) {
        return Employee.builder()
                .floor(floor)
                .name(name)
                .salary(salary)
                .department(employee.getDepartment())
                .project(employee.getProject())
                .build();
    }
}
