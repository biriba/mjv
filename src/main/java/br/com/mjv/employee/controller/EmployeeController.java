package br.com.mjv.employee.controller;

import br.com.mjv.employee.controller.request.EmployeeRequest;
import br.com.mjv.employee.model.Employee;
import br.com.mjv.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping
    @ResponseBody
    public ResponseEntity<Employee> save(@RequestBody EmployeeRequest employeeRequest) {
        return ResponseEntity.ok().body(employeeService.save(employeeRequest));
    }
    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Employee>> getAllEmployees(@PathVariable("page") int page,
                                                          @PathVariable("size") int size,
                                                          @PathVariable("sortDir") String sortDir,
                                                          @PathVariable("sort") String sort){
        List<Employee> list = employeeService.getAllEmployees(page, size, sortDir, sort);
        return new ResponseEntity<List<Employee>>(list, new HttpHeaders(), HttpStatus.OK);
    }
}
