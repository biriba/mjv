package br.com.mjv.employee.service;

import br.com.mjv.employee.controller.request.EmployeeRequest;
import br.com.mjv.employee.model.Employee;
import br.com.mjv.employee.repository.DepartmentRepository;
import br.com.mjv.employee.repository.EmployeeRepository;
import br.com.mjv.employee.repository.ProjectRepository;
import br.com.mjv.employee.service.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final DepartmentRepository departmentRepository;

    private final ProjectRepository projectRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository,
                           DepartmentRepository departmentRepository,
                           ProjectRepository projectRepository) {
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
        this.projectRepository = projectRepository;
    }

    public List<Employee> getAllEmployees(int page, int size, String sortDir, String sort)
    {
        PageRequest pageReq
                = PageRequest.of(page, size, Sort.Direction.fromString(sortDir), sort);
        Page<Employee> posts = employeeRepository.findAll(pageReq);
        return posts.getContent();
    }

    @Transactional
    public Employee save(EmployeeRequest employeeRequest) {

        Employee employee = new Employee();

        Optional.ofNullable(employeeRequest).orElseThrow();

        employee.setDepartment(departmentRepository.findById(employeeRequest.getDepartmentName())
                .orElseThrow(() ->  new BusinessException("Departamento não encontrado", new Throwable("Erro"))));

        employee.setProject(projectRepository.findByName(employeeRequest.getProjectName())
                .orElseThrow(() -> new BusinessException("Projeto não encontrado", new Throwable("Erro"))));
        log.info("");

        return employeeRepository.save(employeeRequest.converter(employee));

    }


}
