package br.com.mjv.employee.model;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Data
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    @NotNull
    private String name;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_name")
    private Department department;
    @OneToOne
    @JoinColumn(name = "project_id")
    private Project project;
    private Integer floor;
    private BigDecimal salary;

}
