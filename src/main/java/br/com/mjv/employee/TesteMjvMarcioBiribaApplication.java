package br.com.mjv.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteMjvMarcioBiribaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteMjvMarcioBiribaApplication.class, args);
	}

}
