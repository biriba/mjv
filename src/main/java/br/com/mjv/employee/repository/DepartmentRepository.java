package br.com.mjv.employee.repository;

import br.com.mjv.employee.model.Department;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DepartmentRepository extends PagingAndSortingRepository<Department,String> {

}
