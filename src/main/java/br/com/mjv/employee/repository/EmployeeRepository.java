package br.com.mjv.employee.repository;

import br.com.mjv.employee.model.Employee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee,Integer> {
    Optional<Employee> findById(String id);
}
