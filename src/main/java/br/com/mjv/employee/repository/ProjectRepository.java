package br.com.mjv.employee.repository;

import br.com.mjv.employee.model.Project;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface ProjectRepository extends PagingAndSortingRepository<Project,Integer> {
    Optional<Project> findByName(String name);
}
